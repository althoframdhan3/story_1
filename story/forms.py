from django import forms
from .models import *

SEMESTER = [
    ('gasal', 'Gasal'),
    ('genap', 'Genap'),
]
TAHUN = [
    ('2020/2021', '2020/2021'),
    ('2021/2022', '2021/2022'),
    ('2022/2023', '2022/2023'),
]


class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = '__all__'
    text_area = {
        'class': "form__control",
        'cols': 90,
        'rows': 5,
        'required': 'required'
    }
    deskripsi_mata_kuliah = forms.CharField(
        widget=forms.Textarea(attrs=text_area))
    semester = forms.ChoiceField(choices=SEMESTER)
    tahun_ajaran = forms.ChoiceField(choices=TAHUN)


class CreateActivityForm(forms.ModelForm):
    class Meta:
        model = Activity
        fields = '__all__'
        labels = {
            'title':'Nama Kegiatan',
            'description': 'Deskripsi kegiatan'
        }
        help_texts = {
            'title': '*max 50 characters',
            'description': '*max 100 characters'
        }

class AddMemberForm(forms.ModelForm):
    class Meta:
        model = Member
        fields = ['name']
        labels = {
            'name':'Nama kamu?'
        }
        help_texts = {
            'name': '*max 50 characters',
        }

from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import Account

class SignupForm(UserCreationForm):
	class Meta(UserCreationForm.Meta):
		fields = UserCreationForm.Meta.fields + ("email",)
		labels = {
			'email': 'Email',
		}

class AccountForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = ['hobby']

class ProfileSettingsForm(forms.ModelForm):
	class Meta:
		model = Account
		fields = '__all__'
		exclude = ['user', 'image']