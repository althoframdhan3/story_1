from django.http.request import HttpRequest
from django.test import TestCase, Client, override_settings
from django.urls import reverse, resolve
from .views import *
from .models import *
from . import forms
from .apps import Story1Config as upc

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC
import time
# Create your tests here.


@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class TestStory(TestCase):

    def test_apps(self):
        self.assertEqual(upc.name, 'story')

    def test_url_about(self):
        response = self.client.get(reverse('story:about'))
        self.assertEqual(response.status_code, 200)

    def test_url_home(self):
        response = self.client.get(reverse('story:home'))
        self.assertEqual(response.status_code, 200)

    def test_url_course(self):
        response = self.client.get(reverse('story:course'))
        self.assertEqual(response.status_code, 200)

    def test_models_course(self):
        obj = Course.objects.create(
            nama_mata_kuliah="mtk",
            jumlah_sks=3
        )
        self.assertEqual(obj.__str__(), "mtk")

    def test_url_create_course(self):
        response = self.client.get(reverse('story:create_course'))
        self.assertEqual(response.status_code, 200)

    def test_url_delete_course(self):
        Course.objects.create(
            nama_mata_kuliah="hiya",
            jumlah_sks=3,
        )
        response = self.client.get(reverse('story:delete_course', args=[1]))
        self.assertEqual(response.status_code, 302)

    def test_url_course_details(self):
        Course.objects.create(
            nama_mata_kuliah="hiya",
            jumlah_sks=3,
        )
        response = self.client.get(reverse('story:course_details', args=[1]))
        self.assertEqual(response.status_code, 200)
        # self.assertEqual(resolve(response).func, course_details)

    def test_url_activity(self):
        response = self.client.get(reverse('story:activity'))
        self.assertEqual(response.status_code, 200)

    def test_views_activity(self):
        response = self.client.get(reverse('story:activity'))

        # url = resolve()
        self.assertTemplateUsed(response, "story/activity.html")

    def test_models_activity(self):
        obj = Activity.objects.create(title="maen", description="lalala")

        Member.objects.create(name="nina", activity=obj)
        self.assertEqual(obj.__str__(), "maen")
        self.assertEqual(Activity.objects.all().count(), 1)
        self.assertEqual(obj.members.all().count(), 1)

        Member.objects.create(name="nina", activity=obj)
        self.assertEqual(obj.members.all().count(), 2)

    def test_template_activity(self):
        obj = Activity.objects.create(title="maen", description="lalala")
        Member.objects.create(name="andi", activity=obj)

        response = activity(HttpRequest)
        html_response = response.content.decode('utf8')
        self.assertIn('andi', html_response)

    def test_models_member(self):
        obj = Activity.objects.create(title="maen", description="lalala")
        member = Member.objects.create(name="andi", activity=obj)
        self.assertEqual(member.__str__(), "andi")

    def test_url_create_activity(self):
        response = self.client.get(reverse('story:create_activity'))
        self.assertEqual(response.status_code, 200)

    def test_url_remove_activity(self):
        Activity.objects.create(title="scsa", description="ajccencwe")
        response = self.client.get(reverse('story:remove_activity', args=[1]))
        self.assertEqual(response.status_code, 302)

    def test_url_add_member(self):
        Activity.objects.create(title="maen", description="lalala")
        response = self.client.get(reverse('story:add_member', args=[1]))
        self.assertEqual(response.status_code, 200)

    def test_delete_member(self):
        obj = Activity.objects.create(title="maen", description="lalala")
        member = Member.objects.create(name="andi", activity=obj)
        response = self.client.get(reverse('story:kick_member', args=[1]))
        self.assertEqual(response.status_code, 302)

    def test_form_add_member(self):
        Activity.objects.create(title="maen", description="lalala")

        form = forms.AddMemberForm(data={'name': "andi"})
        self.assertTrue(form.is_valid())

        response = self.client.post(
            reverse('story:add_member', args=[1]),
            {'name': "andi"}
        )
        self.assertEqual(response.status_code, 302)

    def test_form_create_activity(self):

        form = forms.CreateActivityForm(data={
            'title': "hiyahiya",
            'description': "mantapasik"
        })
        self.assertTrue(form.is_valid())

        response = self.client.post(
            reverse('story:create_activity'),
            {
                'title': "hiyahiya",
                'description': "mantapasik"
            }
        )
        self.assertEqual(response.status_code, 302)

    def test_form_create_course(self):

        data = {
            'nama_mata_kuliah': "hiyahiya",
            'jumlah_sks': 3,
            'dosen_pengajar': "hahah",
            'deskripsi_mata_kuliah': "ajcdjcndce",
            'ruang_kelas': "ejnoencew",
            'semester': "genap",
            'tahun_ajaran': "2021/2022"
        }

        form = forms.CourseForm(data=data)
        self.assertTrue(form.is_valid())

        response = self.client.post(
            reverse('story:create_course'),
            data
        )
        self.assertEqual(response.status_code, 302)

    def test_url_accordion(self):
        url = reverse('story:accordion')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_url_book(self):
        url = reverse('story:book')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_view_book(self):
        url = reverse('story:book')
        response = self.client.get(url)
        self.assertTemplateUsed(response, 'story/book.html')
        self.assertIn("<tr>", response.content.decode('utf8'))

    def test_url_dataBook(self):
        url = reverse('story:dataBook')
        response = self.client.get(url, {'q': 'frozen 2'})
        self.assertEqual(response.status_code, 200)
        self.assertIn("frozen 2", str(response.content.decode('utf8')).lower())

    
    def test_template_register(self):
        hasil = Client().get('/register/')
        self.assertTemplateUsed(hasil, 'story/register.html')

    def test_home_page_register(self):
        hasil = Client().get('/register/')
        self.assertEqual(hasil.status_code, 200)

    def test_home_page_login(self):
        hasil = Client().get('/login/')
        self.assertEqual(hasil.status_code, 200)

    def test_halaman_register_views(self):
        found = resolve('/register/')
        self.assertEqual(found.func, signup_view)

    def test_halaman_logout_views(self):
        found = resolve('/logout/')
        self.assertEqual(found.func, logout_view)

    def test_halaman_login_views(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login_view)

    def test_halaman_profile_views(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, profile_view)

    def test_model(self):
        hasil = User.objects.create_user(
            'john', 'lennon@thebeatles.com', 'johnpassword')
        Account.objects.create(
            user=hasil,
            name=hasil.username,
            email=hasil.email,
            hobby="petani"
        )
        hasil2 = Account.objects.get(id=1)
        self.assertEqual(hasil2.hobby, "petani")
        self.assertEqual(str(hasil2), "john")

    def test_login(self):
        user = User.objects.create_user(
            username='adolf', email='adolfo@na.zi', password='hailhailhitler')
        response = self.client.post(reverse('story:login'), data={
                                    'username': 'adolf', 'password': 'hailhailhitler'})
        self.assertEqual(response.status_code, 302)

    def test_login_failed(self):
        response = self.client.post(reverse('story:login'), data={
                                    'username': '', 'password': ''})
        self.assertContains(response, "Username OR password is incorrect")

    def test_logout(self):
        User.objects.create_user(
            username='adolf', email='adolfo@na.zi', password='hailhailhitler')
        self.client.login(username='adolf', password='hailhailhitler')
        response = self.client.get(reverse('story:logout'), {})
        self.assertEqual(response.status_code, 302)

    def test_signup(self):
        response = self.client.post(reverse('story:register'), data={
            'username': 'mrPutin',
            'email': 'vla@di.mir',
            'password1': 'vivarusia',
            'password2': 'vivarusia',
            'hobby': 'manjat'
        })
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Account.objects.all().count(), 1)

    def test_profile_settings(self):
        user = User.objects.create(username="mrPutin", email='vla@di.mir')
        account = Account.objects.create(
            user=user,
            name=user.username,
            email=user.email,
            hobby='maen'
        )
        user.set_password('vivarusia')
        user.save()

        self.client.login(username='mrPutin', password='vivarusia')
        response = self.client.post(reverse('story:dashboard'), data={
            'name': user.username,
            'email': user.email,
            'hobby': account.hobby,
            'profilepicture-clear': '',
            'profilepicture': '', })
        self.assertContains(response, "ugly")


class TestFunctional(LiveServerTestCase):

    def setUp(self):
        super().setUp()
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(
            executable_path='./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_book_result(self):
        self.selenium.get(self.live_server_url+reverse('story:book'))
        # time.sleep(5)
        input = self.selenium.find_element_by_id('input')
        input.send_keys('frozen 2')
        time.sleep(2)
        self.assertIn('<tr>', self.selenium.page_source)
        self.assertIn(
            'frozen 2',
            str(self.selenium.find_element_by_tag_name('tbody').text).lower()
        )

    def test_accordion(self):
        self.selenium.get(self.live_server_url+reverse('story:accordion'))

        accordion_first = self.selenium.find_element_by_id('accordion-1')
        self.assertIn('banyak dah sibuk takut gamuat 😎',
                      self.selenium.page_source)

        time.sleep(1)

        # self.assertNotIn('banyak dah sibuk bgt pokoknyaaa gamuatttt', self.selenium.page_source)
        accordion_second = self.selenium.find_element_by_id('accordion-2')
        accordion_second.click()
        self.assertIn('berat badan turun 5kg dalam seminggu 🏆🥇',
                      self.selenium.page_source)

        time.sleep(1)

        # self.assertNotIn('gaada :(', self.selenium.page_source)
        accordion_third = self.selenium.find_element_by_id('accordion-3')
        accordion_third.click()
        self.assertIn('the mortal instrument, iron fist, dll. 🎬🎥🍿',
                      self.selenium.page_source)

        time.sleep(1)

        # self.assertNotIn('weeei cek letterboxd guaaaa ayo kita mutualan', self.selenium.page_source)
        accordion_fourth = self.selenium.find_element_by_id('accordion-4')
        accordion_fourth.click()
        self.assertIn('nonton drakor lahh ga sabar liburan 🎊',
                      self.selenium.page_source)
