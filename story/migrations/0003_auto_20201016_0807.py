# Generated by Django 3.0 on 2020-10-16 01:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('story', '0002_auto_20201016_0049'),
    ]

    operations = [
        migrations.RenameField(
            model_name='course',
            old_name='description',
            new_name='deskripsi_mata_kuliah',
        ),
    ]
