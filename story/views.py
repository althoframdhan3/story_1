from django.http import request
from django.http.response import HttpResponse
from django.shortcuts import redirect, render
from . import forms
from .models import *


from django.http import JsonResponse
import json
import requests

# Create your views here.
def home(request):
    return render(request, 'story/story1.html')

def about(request):
    return render(request, 'story/story3.html')

def create_course(request):
    form = forms.CourseForm(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        form.save()
        return redirect('story:course')

    context = {
        "form":form
    }
    return render(request, 'story/create_course.html', context)

def show_course(request):
    matkuls = Course.objects.all().order_by('nama_mata_kuliah')
    context = {
        'matkuls':matkuls
    }
    return render(request, 'story/show_course.html', context)
def course_details(request, matkul_pk):
    matkul = Course.objects.get(id=matkul_pk)
    context = {
        'matkul':matkul
    }
    return render(request, 'story/course_details.html', context)
def delete_course(request, matkul_pk):
    matkul = Course.objects.get(id=matkul_pk)
    matkul.delete()
    return redirect('story:course')

def activity(request):
    activities = Activity.objects.all()
    context = {
        'activities': activities,
    }
    return render(request, 'story/activity.html', context)

def create_activity(request):
    form = forms.CreateActivityForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        form.save()
        return redirect('story:activity')
    context = {
        "form":form
    }
    return render(request, 'story/create_activity.html', context)

def remove_activity(request, activity_id):
    obj = Activity.objects.get(id=activity_id)
    obj.delete()
    return redirect('story:activity')

def add_member(request, activity_id):
    activ = Activity.objects.get(id=activity_id)
    form = forms.AddMemberForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        instance = form.save(commit=False)
        instance.activity = activ
        instance.save()
        return redirect('story:activity')
    context = {
        "form":form,
        'activity': activ
    }
    return render(request, 'story/add_member.html', context)

def kick_member(request, member_id):
    member = Member.objects.get(id=member_id)
    member.delete()
    return redirect('story:activity')

def accordion(request):
    return render(request, 'story/accordion.html')

def book(request):
    return render(request, 'story/book.html')

def dataBook(request):
    q = request.GET.get('q') or 'a'
    api = requests.get('https://www.googleapis.com/books/v1/volumes?q='+q).content
    responseData = json.loads(api)
    return JsonResponse(responseData, safe=False)

from .forms import SignupForm, AccountForm, ProfileSettingsForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
import os
import base64
from ppw2020.settings import MEDIA_ROOT
from django.contrib import messages
# Create your views here.


def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        account_form = AccountForm(request.POST)
        if form.is_valid() and account_form.is_valid():
            user = form.save()
            account = account_form.save(commit=False)
            account.user = user
            account.name = user.username
            account.email = user.email
            account.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('story:dashboard')
    else:
        form = SignupForm()
        account_form = AccountForm()

    context = {'form': form, 'account_form': account_form}
    return render(request, 'story/register.html', context)


@login_required(login_url='/login')
def logout_view(request):
    logout(request)
    return redirect('story:about')


def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return redirect('story:dashboard')
        else:
            messages.info(request, 'Username OR password is incorrect')
    context = {}
    return render(request, 'story/login.html', context)


@login_required(login_url='/login')
# @allowed_users(allowed_roles=['account'])
def profile_view(request):
    account = request.user.account
    form = ProfileSettingsForm(instance=account)

    if request.method == 'POST':
        form = ProfileSettingsForm(
            request.POST, request.FILES, instance=account)
        if form.is_valid():
            ins = form.save()
            if request.FILES.get('profilepicture'):
                file_path = os.path.join(MEDIA_ROOT, str(request.FILES.get('profilepicture')))
                with open(file_path, "rb") as img_file:
                    my_string = "data:image/png;base64,"+str(base64.b64encode(img_file.read()).decode('utf-8'))
                ins.image = my_string
                ins.save()
                messages.warning(request, 'Your profile picture so cool!😎')
            elif request.POST.get('profilepicture-clear') != None:
                ins.image = '/static/story/logo/user.svg'
                ins.save()
                messages.warning(request, 'Your profile picture so ugly!😈')
            else:
                messages.warning(request, 'Profile updated successfully!😊')
            
    context = {'form': form, 'account': account}
    return render(request, 'story/dashboard.html', context)