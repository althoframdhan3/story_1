from django.urls import path, include
from . import views

app_name = "story"

urlpatterns = [
    path("", views.home, name="home"),
    path("about/", views.about, name='about'),
    path("course/", views.show_course, name='course'),
    path("course/create/", views.create_course, name='create_course'),
    path("course/delete/<int:matkul_pk>/", views.delete_course, name='delete_course'),
    path("course/details/<int:matkul_pk>/", views.course_details, name='course_details'),

    path("activity/", views.activity, name="activity"),
    path("activity/create/", views.create_activity, name="create_activity"),
    path("activity/<int:activity_id>/add_member", views.add_member, name="add_member"),
    path("activity/remove/member/<int:member_id>", views.kick_member, name="kick_member"),
    path("activity/remove/<int:activity_id>", views.remove_activity, name="remove_activity"),

    path("accordion/", views.accordion, name="accordion"),

    path("book/", views.book, name="book"),
    path("dataBook/", views.dataBook, name="dataBook"),

    path("login/", views.login_view, name="login"),
    path("register/", views.signup_view, name="register"),
    path("logout/", views.logout_view, name="logout"),
    path("dashboard/", views.profile_view, name="dashboard"),
]
