from django.db import models


# Create your models here.


class Course(models.Model):
    nama_mata_kuliah = models.CharField(max_length=200)
    dosen_pengajar = models.CharField(max_length=200)
    jumlah_sks = models.PositiveSmallIntegerField()
    deskripsi_mata_kuliah = models.TextField()
    semester = models.CharField(max_length=200)
    tahun_ajaran = models.CharField(max_length=200)
    ruang_kelas = models.CharField(max_length=200)

    def __str__(self):
        return self.nama_mata_kuliah


class Activity(models.Model):
    title = models.CharField(max_length=50, blank=False)
    description = models.CharField(max_length=100, blank=False)
    
    def __str__(self):
        return self.title


class Member(models.Model):
    name = models.CharField(max_length=50, blank=False)
    activity = models.ForeignKey(
        Activity,
        related_name="members",
        blank=False,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Account(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	name = models.CharField(max_length=150, null=True)
	email = models.EmailField(max_length=200, null=True)
	hobby = models.CharField(max_length=50, null=True)
	image = models.TextField(default='/static/story/logo/user.svg', null=True, blank=True)
	profilepicture = models.ImageField(default='user.svg', null=True, blank=True)

	def __str__(self):
		return self.user.username